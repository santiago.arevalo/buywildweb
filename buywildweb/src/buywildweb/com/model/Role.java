package buywildweb.com.model;

public class Role {
	
	public static final int Master = 1;
	public static final int Administrator = 2;
	public static final int Traductor = 3;
	public static final int Designer = 4;
	public static final int Editor = 5;
	public static final int Author = 6;
	public static final int Contribuitor = 7;
	public static final int Shop_manager = 8;
	public static final int Customer = 9;
	public static final int Suscribe = 10;
	public static final int User = 11;
	
	public Role() {}
	
	public int getMaster() {
		return Master;
	}
	
	public int getAdministrator() {
		return Administrator;
	}

}
