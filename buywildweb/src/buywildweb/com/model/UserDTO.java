package buywildweb.com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import buywildweb.com.values.Constant;

public class UserDTO {
	
	// table
	public static final String TABLE_USER = "user";
	public static final String TABLE_USER__ID = "_id";
	public static final String TABLE_USER__ROLE = "role";
	public static final String TABLE_USER__NAME = "name";
	public static final String TABLE_USER__PASS = "password";
	public static final String TABLE_USER__HASH = "hash";
	public static final String TABLE_USER__SALT = "salt";
	public static final String TABLE_USER__MAIL = "mail";
	
	// sentences
	public static final String SELECT__FIRST_USER = "SELECT " + TABLE_USER__ID
			+ " FROM " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " WHERE " + TABLE_USER__ID + " = 1;";
	
	public static final String SELECT__ID__BY_NAME = "SELECT " + TABLE_USER__ID
			+ " FROM " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " WHERE " + TABLE_USER__NAME + " LIKE (?);";
	
	public static final String SELECT__ID__BY_MAIL = "SELECT " + TABLE_USER__ID
			+ " FROM " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " WHERE " + TABLE_USER__MAIL + " LIKE (?);";
	
	public static final String SELECT__SALT__BY_NAME = "SELECT " + TABLE_USER__SALT
			+ " FROM " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " WHERE " + TABLE_USER__NAME + " LIKE (?);";
	
	public static final String SELECT__ROLE__BY_SALT_AND_PASS = "SELECT " + TABLE_USER__ROLE
			+ " FROM " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " WHERE " + TABLE_USER__SALT + " LIKE (?) AND " + TABLE_USER__PASS + " LIKE (?);";
	
	public static final String INSERT = "INSERT INTO " + Constant.DATABASE__NAME + Constant.DOT + TABLE_USER
			+ " (" + TABLE_USER__NAME + ", " + TABLE_USER__PASS + ", " + TABLE_USER__ROLE + ", " + TABLE_USER__SALT + ", "
			+ TABLE_USER__MAIL + ") VALUES (?, ?, ?, ?, ?);";
	
	public UserDTO() {}
	
	public int selectFirstUser(Connection connection) {
		int id = Constant.NOT_RESULTS;
		if (connection != null) {
			String query = SELECT__FIRST_USER;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				ResultSet result = sentence.executeQuery();

				// check result
				if (result.next()) {
					id = result.getInt(TABLE_USER__ID);
				}

				// close
				if (!sentence.isClosed()) {
					sentence.close();
				}
				if (!result.isClosed()) {
					result.close();
				}
			} catch (SQLException e) {
				// do nothing (result -1)
			}
		}
		return id;
	}
	
	public int selectIdByName(Connection connection, String name) {
		int id = Constant.NOT_RESULTS;
		if (connection != null) {
			String query = SELECT__ID__BY_NAME;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				sentence.setString(1, name);
				ResultSet result = sentence.executeQuery();

				// check result
				if (result.next()) {
					id = result.getInt(TABLE_USER__ID);
				}

				// close
				if (!sentence.isClosed()) {
					sentence.close();
				}
				if (!result.isClosed()) {
					result.close();
				}
			} catch (SQLException e) {
				// do nothing (result -1)
			}
		}
		return id;
	}
	
	public int selectIdByMail(Connection connection, String mail) {
		int id = Constant.NOT_RESULTS;
		if (connection != null) {
			String query = SELECT__ID__BY_MAIL;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				sentence.setString(1, mail);
				ResultSet result = sentence.executeQuery();

				// check result
				if (result.next()) {
					id = result.getInt(TABLE_USER__ID);
				}

				// close
				if (!sentence.isClosed()) {
					sentence.close();
				}
				if (!result.isClosed()) {
					result.close();
				}
			} catch (SQLException e) {
				// do nothing (result -1)
			}
		}
		return id;
	}
	
	public String selectSaltByName(Connection connection, String name) {
		String salt = Constant.EMPTY_STRING;
		if (connection != null) {
			String query = SELECT__SALT__BY_NAME;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				sentence.setString(1, name);
				ResultSet result = sentence.executeQuery();

				// check result
				if (result.next()) {
					salt = result.getString(TABLE_USER__SALT);
				}

				// close
				if (!sentence.isClosed()) {
					sentence.close();
				}
				if (!result.isClosed()) {
					result.close();
				}
			} catch (SQLException e) {
				// do nothing (empty result)
			}
		}
		return salt;
	}
	
	public int selectRoleBySaltAndPassword(Connection connection, String saltCrypt, String passCrypt) {
		int role = Constant.NOT_RESULTS;
		if (connection != null) {
			String query = SELECT__ROLE__BY_SALT_AND_PASS;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				sentence.setString(1, saltCrypt);
				sentence.setString(2, passCrypt);
				ResultSet result = sentence.executeQuery();

				// check result
				if (result.next()) {
					role = result.getInt(TABLE_USER__ROLE);
				}

				// close
				if (!sentence.isClosed()) {
					sentence.close();
				}
				if (!result.isClosed()) {
					result.close();
				}
			} catch (SQLException e) {
				// do nothing (result -1)
			}
		}
		return role;
	}
	
	public int insertUser(Connection connection, User user) {
		int id = Constant.NOT_RESULTS;
		if (connection != null) {
			String query = INSERT;
			try {
				PreparedStatement sentence = connection.prepareStatement(query);
				// set parameters
				sentence.setString(1, user.getName());
				sentence.setString(2, user.getPassword());
				sentence.setInt(3, user.getRole());
				sentence.setString(4, user.getSaltCrypt());
				sentence.setString(5, user.getMail());

				// execute insert
				int resInsert = sentence.executeUpdate();

				// check insert
				if (resInsert > 0) {
					// get new id inserted
					query = SELECT__ID__BY_NAME;
					sentence = connection.prepareStatement(query);
					// set parameter user name
					sentence.setString(1, user.getName());
					ResultSet result = sentence.executeQuery();

					// check result
					if (result.next()) {
						id = result.getInt(TABLE_USER__ID);
					}

					// close
					if (!sentence.isClosed()) {
						sentence.close();
					}
					if (!result.isClosed()) {
						result.close();
					}

				} else {
					// close
					if (!sentence.isClosed()) {
						sentence.close();
					}
				}
			} catch (SQLException e) {
				// do nothing (result -1)
			}
		}
		return id;
	}

}
