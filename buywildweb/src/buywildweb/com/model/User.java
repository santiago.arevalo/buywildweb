package buywildweb.com.model;

public class User {
	
	// - - - - - A T R I B U T O S - - - - -
	private String name;
	private byte[] salt;
	private byte[] hash;
	private String passwordCrypt;
	String saltCrypt;
	private String mail;
	private int role;

	// - - - - - C O N S T R U C T O R E S - - - - -
	public User(String name, String pass) {
		this.name = name;
		this.passwordCrypt = pass;
	}
		
	public User(String name, String pass, String email) {
		this.name = name;
		this.passwordCrypt = pass;
		this.mail = email;
	}

	// - - - - - - M É T O D O S - - - - -
	public String getName() {
		return name;
	}

	public String getPassword() {
		return passwordCrypt;
	}

	public String getMail() {
		return mail;
	}

	public byte[] getSalt() {
		return salt;
	}

	public byte[] getHash() {
		return hash;
	}

	public String getSaltCrypt() {
		return saltCrypt;
	}
	
	public int getRole() {
		return role;
	}
	
	public void setRole(int role) {
		this.role = role;
	}
	
	public void setSaltCrypt(String saltCrypt) {
		this.saltCrypt = saltCrypt;
	}
	
	public void setPassCrypt(String passCrypt) {
		this.passwordCrypt = passCrypt;
	}

}
