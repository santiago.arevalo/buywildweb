package buywildweb.com.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;

public class ConnectionDB {
	
	/**
	 * - - - - - A T T R I B U T E S - - - - -
	 */
	private String driver;
	private String prefixServer;
	private String servername;
	private String port;
	private String dataBase;
	private String schema;
	private String user;
	private String password;
	
	/**
	 * - - - - - C O N S T R U C T O R S - - - - -
	 */
	/**
	 * Default constructor.
	 * 
	 * Create default connection.
	 */
	public ConnectionDB() {
		this.driver = Constant.CONNECTION__DRIVER;
		this.prefixServer = Constant.CONNECTION__PREFIX;
		this.servername = null;
		this.port = null;
		this.dataBase = null;
		this.schema = null;
		this.user = null;
		this.password = null;
	}
	
	/**
	 * Override constructor.
	 * 
	 * Create a concrete connection.
	 * 
	 * @param driver
	 *            name of the driver
	 * @param prefixServer
	 *            prefix to the server name
	 * @param servername
	 *            the server name
	 * @param port
	 *            port
	 * @param dataBase
	 *            data base name
	 * @param schema
	 *            schema name
	 * @param user
	 *            user name
	 * @param password
	 *            user password
	 */
	public ConnectionDB(String driver, String prefixServer, String servername, String port,
			String dataBase, String schema, String user, String password) {
		this.driver = driver;
		this.prefixServer = prefixServer;
		this.servername = servername;
		this.port = port;
		this.dataBase = dataBase;
		this.schema = schema;
		this.user = user;
		this.password = password;
	}
	
	/**
	 * - - - - - M E T H O D S - - - - -
	 */
	/**
	 * Get database connection from Session.
	 * 
	 * @param sesion
	 * 
	 * @return database connection or null if connection failed.
	 */
	public Connection getConnectionFromSession(HttpSession sesion) {
		Connection connection = null;
		// get connection attribute from session
		Enumeration<String> attributeArray = sesion.getAttributeNames();
		while (attributeArray.hasMoreElements()) {
			String attribute = attributeArray.nextElement();
			if (attribute.equals(KeyConstant.CONNECTION_DB)) {
				// set connection
				connection = (Connection) sesion.getAttribute(KeyConstant.CONNECTION_DB);
			}
		}

		// check connection
		if (connection == null) {
			connection = getSavedConnection();
		}
		return connection;
	}
	
	/**
	 * Get database connection from attributes.
	 * 
	 * Create and return a database connection.
	 * 
	 * @return database connection or null if connection failed.
	 */
	public Connection getConnection() {
		Connection connection = null;

		// set url and properties
		String url = prefixServer + servername + Constant.COLON + port + Constant.SLASH + dataBase;
		Properties properties = new Properties();
		properties.setProperty(KeyConstant.SERVER__USER, user);
		properties.setProperty(KeyConstant.SERVER__PASSWORD, password);
		properties.setProperty(KeyConstant.SERVER__CURRENT_SCHEMA, schema);
		properties.setProperty(KeyConstant.SERVER__CONNECTION_TIMEOUT, Constant.CONNECTION__TIMEOUT__CONNECTION);
		properties.setProperty(KeyConstant.SERVER__SOCKET_TIMEOUT, Constant.CONNECTION__TIMEOUT__SOCKET);

		// create new connection
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, properties);
		} catch (ClassNotFoundException e) {
			return null; // driver postgresql failed
		} catch (SQLException e) {
			return null; // created connection failed
		}

		return connection;
	}
	
	/**
	 * Get new database connection.
	 * 
	 * Create and return a new database connection.
	 * 
	 * @return database connection or null if connection failed.
	 */
	public Connection getNewConnection(String servername, String port,
			String dataBase, String schema, String user, String password) {
		Connection connection = null;

		// set url and properties
		String url = prefixServer + servername + Constant.COLON + port + Constant.SLASH + dataBase;
		Properties properties = new Properties();
		properties.setProperty(KeyConstant.SERVER__USER, user);
		properties.setProperty(KeyConstant.SERVER__PASSWORD, password);
		properties.setProperty(KeyConstant.SERVER__CURRENT_SCHEMA, schema);
		properties.setProperty(KeyConstant.SERVER__CONNECTION_TIMEOUT, Constant.CONNECTION__TIMEOUT__CONNECTION);
		properties.setProperty(KeyConstant.SERVER__SOCKET_TIMEOUT, Constant.CONNECTION__TIMEOUT__SOCKET);

		// create new connection
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, properties);
		} catch (ClassNotFoundException e) {
			return null; // driver postgresql failed
		} catch (SQLException e) {
			return null; // created connection failed
		}

		return connection;
	}
	
	/**
	 * Get database connection from data file saved.
	 * 
	 * @return database connection or null if connection failed
	 */
	public Connection getSavedConnection() {
		// create new file
		File f = new File(Constant.filename);
		try {
			if (!f.createNewFile()) {
				// if file exist, check correct data server
				String line = null;
				String dataserver = Constant.EMPTY_STRING;
				int count = Constant.ZERO;
				BufferedReader bf = new BufferedReader(new FileReader(f));
				while ((line = bf.readLine()) != null) {
					dataserver = dataserver.concat(Constant.LINE_BREAK + line);
					count++;
				}
				bf.close();
				if (count > Constant.ZERO && !dataserver.isEmpty()) {
					servername = null;
					port = null;
					dataBase = null;
					schema = null;
					user = null;
					password = null;
					StringTokenizer st = new StringTokenizer(dataserver, Constant.LINE_BREAK);
					String token = null;
					String arrayToken[];
					while (st.hasMoreTokens()) {
						token = st.nextToken();
						arrayToken = token.split(Constant.COLON);
						switch (arrayToken[0]) {
						case KeyConstant.SERVER__HOST: {
							servername = arrayToken[1];
							break;
						}
						case KeyConstant.SERVER__PORT: {
							port = arrayToken[1];
							break;
						}
						case KeyConstant.SERVER__DATABASE: {
							dataBase = arrayToken[1];
							break;
						}
						case KeyConstant.SERVER__SCHEMA: {
							schema = arrayToken[1];
							break;
						}
						case KeyConstant.SERVER__USER: {
							user = arrayToken[1];
							break;
						}
						case KeyConstant.SERVER__PASSWORD: {
							password = arrayToken[1];
							break;
						}
						default:
							break;
						}
					}
					// check correct data server
					if (servername != null && !servername.isEmpty() && port != null && !port.isEmpty() && dataBase != null
							&& !dataBase.isEmpty() && schema != null && !schema.isEmpty() && user != null
							&& !user.isEmpty() && password != null && !password.isEmpty()) {
						if (getConnection() == null) {
							// delete file and repeat process
							f.delete();
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {}
		
		return getConnection();
	}
	
}
