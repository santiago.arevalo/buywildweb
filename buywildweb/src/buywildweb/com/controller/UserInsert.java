package buywildweb.com.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.util.Base64;
import java.util.Enumeration;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import buywildweb.com.model.ConnectionDB;
import buywildweb.com.model.Role;
import buywildweb.com.model.User;
import buywildweb.com.model.UserDTO;
import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;
import buywildweb.com.values.UrlConstant;

/**
 * Insert new User on database.
 */
@WebServlet(Constant.SLASH + UrlConstant.SERVLET__USER_INSERT)
public class UserInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInsert() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String error = ResourceBundle
				.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
				.getString(KeyConstant.ERROR__GENERAL);
		request.setAttribute(KeyConstant.ERROR, error);
		request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// check connection
		HttpSession session = request.getSession();
		Connection connection = new ConnectionDB().getConnectionFromSession(session);
		if (connection != null) {
			
			// get form parameters
			String name = request.getParameter(KeyConstant.USER__NAME);
			String pass = request.getParameter(KeyConstant.USER__PASS);
			String pass2 = request.getParameter(KeyConstant.USER__PASS_2);
			String correo = request.getParameter(KeyConstant.USER__EMAIL);
			String roleString = request.getParameter(KeyConstant.USER__ROLE);
			
			// check same passwords
			if (pass.equals(pass2)) {
				
				int role = Constant.NOT_RESULTS;
				if (roleString != null && !roleString.isEmpty()) {
					role = Integer.parseInt(roleString);
				}

				// create new user
				User user = new User(name, pass, correo);
				UserDTO userDTO = new UserDTO();
				
				// check if role exists
				int id = Constant.NOT_RESULTS;
				if (role > Constant.ZERO) {
					user.setRole(role);
				} else {
					// check if first user
					id = userDTO.selectFirstUser(connection);
					if (id == Constant.NOT_RESULTS) {
						user.setRole(Role.Master); // first user
					} else {
						user.setRole(Role.User);
					}
				}
				
				// check if user name already exist
				id = userDTO.selectIdByName(connection, user.getName());
				if (id == Constant.NOT_RESULTS) {
					
					// check if user mail already exist
					id = userDTO.selectIdByMail(connection, user.getMail());
					if (id == Constant.NOT_RESULTS) {
						
						// encrypt pass empty
						byte[] hash = null;
						String passCrypt = Constant.EMPTY_STRING;
						String saltCrypt = Constant.EMPTY_STRING;
						
						// create random salt
						SecureRandom random = new SecureRandom();
						byte[] salt = new byte[16];
						random.nextBytes(salt);
						saltCrypt = Base64.getEncoder().encodeToString(salt);
						
						// set saltCrypt at user
						user.setSaltCrypt(saltCrypt);
						
						try {
							// encrypt SHA-512
							MessageDigest md = MessageDigest.getInstance(Constant.ENCRYPT_CODE);

							// update encrypt SHA-512 with random salt
							md.update(salt);

							// get encrypt pass
							hash = md.digest(pass.getBytes(StandardCharsets.UTF_8));

							// check encrypt pass
							if (hash != null) {

								// get check encrypt pass on base64
								passCrypt = Base64.getEncoder().encodeToString(hash);
								
								// set final passwordCrypt at user
								user.setPassCrypt(passCrypt);
								
								// insert new user
								int idUsuario = userDTO.insertUser(connection, user);
								
								// check insert
								if (idUsuario != Constant.NOT_RESULTS) {

									// set current session
									session.setAttribute(KeyConstant.USER__NAME, user.getName());
									session.setAttribute(KeyConstant.USER__ROLE, user.getRole());

									// go to home
									request.getRequestDispatcher(UrlConstant.SERVLET__INIT).forward(request, response);

								} else {
									showError(request, response, KeyConstant.ERROR__INSERT_USER__FAILED);
								}
							}
						} catch (NoSuchAlgorithmException e) {
							showError(request, response, KeyConstant.ERROR__INSERT_USER__ENCRYPT_PASSWORD);
						}
					} else {
						showError(request, response, KeyConstant.ERROR__INSERT_USER__MAIL_EXIST);
					}
				} else {
					showError(request, response, KeyConstant.ERROR__INSERT_USER__NAME_EXIST);
				}
			} else {
				showError(request, response, KeyConstant.ERROR__LOGIN__PASSWORD_CONFIRM_FAILED);
			}
		} else {
			// go to init connection for set new connection
			request.getRequestDispatcher(UrlConstant.SERVLET__INIT).forward(request, response);
		}		
	}
	
	private void showError(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
		String error = ResourceBundle
				.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
				.getString(message);
		request.setAttribute(KeyConstant.ERROR, error);
		request.getRequestDispatcher(UrlConstant.JSP__USER_REGISTER).forward(request, response);
	}

}
