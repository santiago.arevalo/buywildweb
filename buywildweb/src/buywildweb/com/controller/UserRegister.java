package buywildweb.com.controller;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;
import buywildweb.com.values.UrlConstant;

/**
 * Form create new User on database.
 */
@WebServlet(Constant.SLASH + UrlConstant.SERVLET__USER_REGISTER)
public class UserRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegister() {
        super();
    }

	/**
	 * Go to error page
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String error = ResourceBundle
				.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
				.getString(KeyConstant.ERROR__GENERAL);
		request.setAttribute(KeyConstant.ERROR, error);
		request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
	}

	/**
	 * Go to register user form view
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(UrlConstant.JSP__USER_REGISTER).forward(request, response);
	}

}
