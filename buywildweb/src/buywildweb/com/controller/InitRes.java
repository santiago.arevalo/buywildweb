package buywildweb.com.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import buywildweb.com.model.ConnectionDB;
import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;
import buywildweb.com.values.UrlConstant;

/**
 * Check if connection server it's OK and redirect to next step:
 * <ul>
 * <li>- INIT form is data server OK</li>
 * <li>- HOME if data server failed</li>
 * </ul>
 */
@WebServlet(Constant.SLASH + UrlConstant.SERVLET__INIT_RES)
public class InitRes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitRes() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String error = ResourceBundle
				.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
				.getString(KeyConstant.ERROR__GENERAL);
		request.setAttribute(KeyConstant.ERROR, error);
		request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// set current locale language
		request.setAttribute(KeyConstant.LOCALE, request.getLocale().getLanguage());
		
		// get server parameters
		String host = request.getParameter(KeyConstant.SERVER__HOST);
		String port = request.getParameter(KeyConstant.SERVER__PORT);
		String database = request.getParameter(KeyConstant.SERVER__DATABASE);
		String schema = request.getParameter(KeyConstant.SERVER__SCHEMA);
		String user = request.getParameter(KeyConstant.SERVER__USER);
		String password = request.getParameter(KeyConstant.SERVER__PASSWORD);
		
		// get connection
		Connection connection = new ConnectionDB().getNewConnection(host, port, database, schema, user, password);
		// check connection
		if (connection == null) {
			// reload with error message
			String error = ResourceBundle
					.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
					.getString(KeyConstant.ERROR__SERVER__CONNECTION_FAILED);
			request.setAttribute(KeyConstant.ERROR, error);
			request.getRequestDispatcher(UrlConstant.JSP__INIT).forward(request, response);
		} else {
			// go login form
			request.setAttribute(KeyConstant.SERVER__CONNECTION, connection);
			request.getRequestDispatcher(UrlConstant.JSP__HOME).forward(request, response);
		}
	}

}
