package buywildweb.com.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Base64;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import buywildweb.com.model.ConnectionDB;
import buywildweb.com.model.User;
import buywildweb.com.model.UserDTO;
import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;
import buywildweb.com.values.UrlConstant;

/**
 * Check user login and redirect correct site.
 */
@WebServlet(Constant.SLASH + UrlConstant.SERVLET__LOGIN)
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String error = ResourceBundle
				.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
				.getString(KeyConstant.ERROR__GENERAL);
		request.setAttribute(KeyConstant.ERROR, error);
		request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get form parameters
		String name = request.getParameter(KeyConstant.USER__NAME);
		String password = request.getParameter(KeyConstant.USER__PASS);
		User user = new User(name, password);
		
		// check connection
		HttpSession session = request.getSession();
		Connection connection = new ConnectionDB().getConnectionFromSession(session);
		if (connection != null) {
			
			// get user salt
			UserDTO userDTO = new UserDTO();
			String saltCrypt = userDTO.selectSaltByName(connection, name);

			// check salt
			if (!saltCrypt.isEmpty()) {

				// get salt
				byte[] salt = Base64.getDecoder().decode(saltCrypt.getBytes());
				try {
					// encrypt
					MessageDigest md = MessageDigest.getInstance(Constant.ENCRYPT_CODE);

					// update encrypt with salt
					md.update(salt);

					// get hash
					byte[] hash = md.digest(user.getPassword().getBytes(StandardCharsets.UTF_8));
					if (hash != null) {

						// get passwordCrypt
						String passCrypt = Base64.getEncoder().encodeToString(hash);

						// get role
						int role = userDTO.selectRoleBySaltAndPassword(connection, saltCrypt, passCrypt);
						user.setRole(role);
						
						if (user.getRole() == Constant.NOT_RESULTS) {
							// show error
							String error = ResourceBundle
									.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
									.getString(KeyConstant.ERROR__LOGIN__PASSWORD_NOT_EXIST);
							request.setAttribute(KeyConstant.ERROR, error);
							request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
						} else {
							session.setAttribute(KeyConstant.USER__NAME, user.getName());
							session.setAttribute(KeyConstant.USER__ROLE, user.getRole());
							RequestDispatcher vista = null;
							if (user.getRole() >= 2) {
								vista = request.getRequestDispatcher(UrlConstant.SERVLET__INIT); // go to backend
							} else {
								vista = request.getRequestDispatcher(UrlConstant.SERVLET__INIT);
							}
							vista.forward(request, response);
						}
					}
				} catch (NoSuchAlgorithmException e) {}
			} else {
				// show error
				String error = ResourceBundle
						.getBundle(UrlConstant.PROPERTY__ERROR, request.getLocale())
						.getString(KeyConstant.ERROR__LOGIN__NAME_NOT_EXIST);
				request.setAttribute(KeyConstant.ERROR, error);
				request.getRequestDispatcher(UrlConstant.JSP__ERROR).forward(request, response);
			}
		} else {
			// go to init connection for set new connection
			request.getRequestDispatcher(UrlConstant.SERVLET__INIT).forward(request, response);
		}
	}

}
