package buywildweb.com.controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import buywildweb.com.model.ConnectionDB;
import buywildweb.com.values.Constant;
import buywildweb.com.values.KeyConstant;
import buywildweb.com.values.UrlConstant;

/**
 * Check if connection server exists and redirect to next step:
 * <ul>
 * <li>- INIT form is data server not exist</li>
 * <li>- HOME if data server exist</li>
 * </ul>
 */
@WebServlet(Constant.SLASH + UrlConstant.SERVLET__INIT)
public class Init extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public Init() {
    	super();
    }

	/**
	 * Run init view.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// set current locale language
		request.setAttribute(KeyConstant.LOCALE, request.getLocale().getLanguage());
		// check data server saved
		Connection connection = new ConnectionDB().getSavedConnection();
		if (connection != null) {
			// go login form
			request.setAttribute(KeyConstant.SERVER__CONNECTION, connection);
			request.getRequestDispatcher(UrlConstant.JSP__HOME).forward(request, response);
		} else {
			// go init form
			request.getRequestDispatcher(UrlConstant.JSP__INIT).forward(request, response);
		}
	}

	/**
	 * Run doGet().
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
