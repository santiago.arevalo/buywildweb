package buywildweb.com.values;

public class Constant {
	
	// file
	public static final String filename = "dataserver.txt";
	
	// general
	public static final String APP_TITLE = "BuyWildWeb";
	public static final String EMPTY_STRING = "";
	public static final int ZERO = 0;
	public static final String LINE_BREAK = "\n";
	public static final String COLON = ":";
	public static final String SLASH = "/";
	public static final String CLOSE_CHAR = "x";
	public static final String DOT = ".";
	public static final int NOT_RESULTS = -1;
	public static final String ENCRYPT_CODE = "SHA-512";
	
	// database default
	public static final String CONNECTION__DRIVER = "org.postgresql.Driver";
	public static final String CONNECTION__PREFIX = "jdbc:postgresql://";
	public static final String CONNECTION__TIMEOUT__CONNECTION = "3";
	public static final String CONNECTION__TIMEOUT__SOCKET = "3";
	public static final String DATABASE__NAME = "buywildweb";
	
}
