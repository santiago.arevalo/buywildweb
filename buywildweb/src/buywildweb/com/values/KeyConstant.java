package buywildweb.com.values;

public class KeyConstant {
	
	// response
	public static final String LOCALE = "locale";
	public static final String ERROR = "error";
	public static final String CONNECTION_DB = "connection_db";
	
	// error
	public static final String ERROR__GENERAL = "GENERAL__ERROR";
	public static final String ERROR__SERVER__CONNECTION_FAILED = "ERROR__SERVER__CONNECTION_FAILED";
	public static final String ERROR__INSERT_USER__NAME_EXIST = "ERROR__USER__NAME_EXISTS";
	public static final String ERROR__LOGIN__NAME_NOT_EXIST = "ERROR__USER__NAME_NOT_EXISTS";
	public static final String ERROR__LOGIN__PASSWORD_NOT_EXIST = "ERROR__USER__PASSWORD_NOT_EXISTS";
	public static final String ERROR__LOGIN__PASSWORD_CONFIRM_FAILED = "ERROR__USER__PASSWORD_CONFIRM_FAILED";
	public static final String ERROR__INSERT_USER__MAIL_EXIST = "ERROR__USER__MAIL_EXISTS";
	public static final String ERROR__INSERT_USER__FAILED = "ERROR__USER__INSERT_FAILED";
	public static final String ERROR__INSERT_USER__ENCRYPT_PASSWORD = "ERROR__USER__PASSWORD_ENCRYPT";
	
	// dataserver
	public static final String SERVER__CONNECTION = "KEY__SERVER_CONNECTION";
	public static final String SERVER__HOST = "host";
	public static final String SERVER__PORT = "port";
	public static final String SERVER__DATABASE = "database";
	public static final String SERVER__SCHEMA = "schema";
	public static final String SERVER__USER = "user";
	public static final String SERVER__PASSWORD = "password";
	public static final String SERVER__CURRENT_SCHEMA = "currentSchema";
	public static final String SERVER__CONNECTION_TIMEOUT = "connectTimeout";
	public static final String SERVER__SOCKET_TIMEOUT = "socketTimeout";
	
	// user
	public static final String USER__NAME = "username";
	public static final String USER__EMAIL = "useremail";
	public static final String USER__PASS = "userpass";
	public static final String USER__PASS_2 = "userpass2";
	public static final String USER__ROLE = "userrole";

}
