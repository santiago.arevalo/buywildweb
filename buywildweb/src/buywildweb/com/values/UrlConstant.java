package buywildweb.com.values;

public class UrlConstant {
	
	// controller
	public static final String SERVLET__INIT = "Init";
	public static final String SERVLET__INIT_RES = "InitRes";
	public static final String SERVLET__LOGIN = "Login";
	public static final String SERVLET__LOGOUT = "Logout";
	public static final String SERVLET__CART = "Cart";
	public static final String SERVLET__USER_REGISTER = "UserRegister";
	public static final String SERVLET__USER_INSERT = "UserInsert";
	public static final String SERVLET__SHOE_REGISTER = "ShoeRegister";
	
	// view
	public static final String JSP__INIT = "view/init.jsp";
	public static final String JSP__ERROR = "view/error.jsp";
	public static final String JSP__HOME = "view/home.jsp";
	public static final String JSP__MODAL_LOGIN = "modal_login.jsp";
	public static final String JSP__MODAL_LOGUED = "modal_logued.jsp";
	public static final String JSP__MODAL_LOGOUT = "modal_logout.jsp";
	public static final String JSP__LOGIN = "view/login.jsp";
	public static final String JSP__USER_REGISTER = "view/user_register.jsp";
	public static final String JSP__SIDEBAR = "sidebar.jsp";
	
	// resource
	public static final String CSS__BOOTSTRAP = "resources/css/bootstrap.min.css";
	public static final String CSS__FONTAWESOME = "resources/css/fontawesome-all.css";
	public static final String CSS__STYLES = "resources/css/styles.css";
	public static final String CSS__ERROR = "resources/css/error.css";
	public static final String CSS__USER_REGISTER = "resources/css/user-register.css";
	public static final String CSS__SIDEBAR = "resources/css/sidebar.min.css";
	public static final String CSS__SIDEBAR_CUSTOM = "resources/css/sidebar-custom.css";
	public static final String CSS__MODAL_LOGIN = "resources/css/modal-login.css";
	
	public static final String JS__JQUERY = "resources/js/jquery-3.4.1.min.js";
	public static final String JS__POPPER = "resources/js/popper.min.js";
	public static final String JS__BOOTSTRAP = "resources/js/bootstrap.min.js";
	public static final String JS__SIDEBAR = "resources/js/sidebar.min.js";
	
	// traducible
	public static final String PROPERTY__ERROR = "error.Errors";
	public static final String PROPERTY__STRING = "string.Strings";

}
