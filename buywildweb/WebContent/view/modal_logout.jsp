<%@page import="buywildweb.com.values.UrlConstant"%>
<%@page import="buywildweb.com.values.Constant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="logoutModalLabel"><fmt:message key="LOGOUT_FORM__SURE"/></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><%=Constant.CLOSE_CHAR%></span>
				</button>
			</div>
			<div class="modal-body"><fmt:message key="LOGOUT_FORM__INFO"/></div>
			<div class="modal-footer">
				<form action="<%=UrlConstant.SERVLET__LOGOUT%>" method="post">
					<input type="button" class="btn btn-secondary" data-dismiss="modal" value="<fmt:message key="LOGOUT_FORM__CANCEL"/>">
					<input type="submit" class="btn btn-primary" value="<fmt:message key="LOGOUT_FORM__LOGOUT"/>">
				</form>
			</div>
		</div>
	</div>
</div>