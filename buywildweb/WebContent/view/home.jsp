<%@page import="buywildweb.com.values.KeyConstant"%>
<%@page import="buywildweb.com.model.Role"%>
<%@page import="buywildweb.com.values.UrlConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="author" content="Santiago Arévalo Lavi">
<link rel="stylesheet" type="text/css" href="<%= UrlConstant.CSS__BOOTSTRAP %>">
<link rel="stylesheet" type="text/css" href="<%= UrlConstant.CSS__FONTAWESOME %>">
<link rel="stylesheet" type="text/css" href="<%= UrlConstant.CSS__SIDEBAR %>">
<link rel="stylesheet" type="text/css" href="<%= UrlConstant.CSS__SIDEBAR_CUSTOM %>">
<link rel="stylesheet" type="text/css" href="<%= UrlConstant.CSS__STYLES %>">
<title>BWW-HOME</title>
</head>
<body id="page-top">

	<!-- wrapper page -->
	<div id="wrapper">
	
	

		<!-- SIDEBAR (if admin or super role) -->
		<c:set var="admin" scope="session" value="<%= Role.Administrator %>"/>
		<c:if test="${sessionScope.userrole <= admin}">
			<jsp:include page="<%= UrlConstant.JSP__SIDEBAR %>"></jsp:include>
		</c:if>

		<!-- wrapper content -->
		<div id="content-wrapper" class="d-flex flex-column">
	
			<!-- main content -->
			<div id="content">
			
				<!-- HEADER -->
				<jsp:include page="header.jsp"></jsp:include>
				
				<!-- CONTENT -->
				<div class="cointainer-fluid">
				
				
				
				</div>
				<!-- END fluid content -->
				
				<!-- FOOTER -->
		
			</div>
			<!-- END main content -->
		
			

		</div>
		<!-- END wrapper content -->

  </div>
  <!-- END wrapper page -->
  
  <!-- scroll button to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<script src="<%= UrlConstant.JS__JQUERY %>"></script>
<script src="<%= UrlConstant.JS__POPPER %>"></script>
<script src="<%= UrlConstant.JS__BOOTSTRAP %>"></script>
<script src="<%= UrlConstant.JS__SIDEBAR %>"></script>
</body>
</html>