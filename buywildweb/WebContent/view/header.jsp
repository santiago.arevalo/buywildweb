<%@page import="buywildweb.com.values.Constant"%>
<%@ page import="buywildweb.com.values.UrlConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="resources/css/header.css">

<!-- LANGUAGE FROM LOCALE -->
<c:if test="${requestScope.locale == 'es'}">
	<c:set var="locale" value="es_ES" />
</c:if>
<c:if test="${requestScope.locale == 'en'}">
	<c:set var="locale" value="en_EN" />
</c:if>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="string.Strings" scope="session" />

<nav class="navbar navbar-expand-sm navbar-dark justify-content-between">

	<!-- MENU ITEMS -->
	<div class="navbar-nav">
	
		<form action="<%=UrlConstant.SERVLET__INIT%>" method="post">
			<button type="submit" class="nav-item nav-link btn-nav-link"><fmt:message key="MENU__HOME"/></button>
		</form>
		
		<form action="<%=UrlConstant.SERVLET__CART%>" method="post">
			<button type="submit" class="nav-item nav-link btn-nav-link"><fmt:message key="MENU__CART"/></button>
		</form>
	</div>
	
	<!-- LOGIN WINDOW, if not login -->
	<c:if test="${sessionScope.username == null}">
		<jsp:include page="<%=UrlConstant.JSP__MODAL_LOGIN%>"></jsp:include>
	</c:if>
	
	<!-- LOGUED WINDOW, if logued -->
	<c:if test="${sessionScope.username != null}">
		<jsp:include page="<%=UrlConstant.JSP__MODAL_LOGUED%>"></jsp:include>
	</c:if>

</nav>

<!-- LOGOUT WINDOW, if logued -->
<c:if test="${sessionScope.username != null}">
	<jsp:include page="<%=UrlConstant.JSP__MODAL_LOGOUT%>"></jsp:include>
</c:if>