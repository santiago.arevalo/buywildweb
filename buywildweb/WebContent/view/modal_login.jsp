<%@page import="buywildweb.com.values.KeyConstant"%>
<%@page import="buywildweb.com.values.UrlConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__MODAL_LOGIN%>">

<!-- LANGUAGE FROM LOCALE -->
<c:if test="${requestScope.locale == 'es'}">
	<c:set var="locale" value="es_ES" />
</c:if>
<c:if test="${requestScope.locale == 'en'}">
	<c:set var="locale" value="en_EN" />
</c:if>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="string.Strings" scope="session" />

<!-- DIV MODAL FOR SIGN IN (fade class for shown with animation) -->
<div class="modal fade" id="modalLogin">
	<!-- DIALOG -->
	<div class="modal-dialog">
		<!-- CONTENT -->
		<div class="modal-content">

			<!-- HEADER -->
			<div class="modal-header">
				<!-- title -->
				<h4 class="modal-title"><fmt:message key="LOGIN_FORM__SIGN_IN"/></h4>
				<!-- X button for close dialog (data-dismiss = modal) -->
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- BODY -->
			<div class="modal-body">
				<div class="card">
					<div class="card-body">
						<!-- form -->
						<form action="<%=UrlConstant.SERVLET__LOGIN%>" method="post" accept-charset="UTF-8">
							<!-- Name -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" class="form-control"
									placeholder="<fmt:message key="LOGIN_FORM__HOLDER_NAME"/>"
									name="<%=KeyConstant.USER__NAME%>">
							</div>
							<!-- Password -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" class="form-control"
									placeholder="<fmt:message key="LOGIN_FORM__HOLDER_PASSWORD"/>"
									name="<%=KeyConstant.USER__PASS%>">
							</div>
							<!-- Remember -->
							<div class="row align-items-center justify-content-center remember">
								<input id="btn_remember" type="checkbox" class="form-check-input check-login" />
								<label class="form-check-label check-login" for="btn_remember"><fmt:message key="LOGIN_FORM__REMEMBER_ME"/></label>
								<input type="submit" value="<fmt:message key="LOGIN_FORM__LOG_IN"/>" class="btn btn-info float-right" />
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- FOOTER -->
			<div class="modal-footer">
				<form action="<%=UrlConstant.SERVLET__USER_REGISTER%>" method='post' accept-charset='UTF-8'>
					<input type="submit" value="<fmt:message key="LOGIN_FORM__REGISTER"/>" class="btn btn-info btn-login-pie" />
					<input id="btnCerrar" type="button" value="<fmt:message key="CLOSE"/>" class="btn btn-danger btn-login-pie" data-dismiss="modal" />
				</form>
			</div>
		</div>
	</div>
</div>

<!-- LOGIN BUTTON -->
<button class="navbar-brand btn-sesion" data-toggle="modal" data-target="#modalLogin">
	<i class="fas fa-user btn-icono-login"></i>
</button>