<%@ page import="buywildweb.com.values.UrlConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="refresh" content="3; url=Init" />
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__STYLES%>">
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__ERROR%>">
<title>BWW-ERROR</title>
</head>
<body>

	<!-- LANGUAGE FROM LOCALE -->
	<c:if test="${requestScope.locale == 'es'}">
		<c:set var="locale" value="es_ES" />
	</c:if>
	<c:if test="${requestScope.locale == 'en'}">
		<c:set var="locale" value="en_EN" />
	</c:if>
	<fmt:setLocale value="${locale}" scope="session" />
	<fmt:setBundle basename="error.Errors" scope="session" />

	<!-- Check error from request -->
	<c:if test="${requestScope.error != null}">
		<div class="container">
			<h1>${requestScope.error}</h1>
		</div>
	</c:if>
	<c:if test="${requestScope.error == null}">
		<div class="container">
		<h1><fmt:message key="GENERAL__ERROR"/></h1>
		</div>
	</c:if>

</body>
</html>