<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<ul class="navbar-nav">
			
	<!-- Nav Item - Usuario -->
	<li class="nav-item dropdown no-arrow">
		<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="mr-2 d-none d-lg-inline text-gray-600 small"><fmt:message key="HEADER__WELCOME"/> ${sessionScope.username}</span>
			<img class="img-profile rounded-circle"	src="https://source.unsplash.com/QAB-WJcbgJk/40x40">
		</a>
		<!-- Usuario - Ventana -->
		<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			<a class="dropdown-item" href="#">
				<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i> <fmt:message key="HEADER__PROFILE"/>
			</a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
				<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> <fmt:message key="HEADER__LOGOUT"/>
			</a>
		</div>
	</li>
			
</ul>