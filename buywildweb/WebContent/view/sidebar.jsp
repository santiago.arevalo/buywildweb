<%@ page import="buywildweb.com.values.Constant" %>
<%@ page import="buywildweb.com.values.UrlConstant" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- LANGUAGE FROM LOCALE -->
<c:if test="${requestScope.locale == 'es'}">
	<c:set var="locale" value="es_ES" />
</c:if>
<c:if test="${requestScope.locale == 'en'}">
	<c:set var="locale" value="en_EN" />
</c:if>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="string.Strings" scope="session" />

<!-- Barra Lateral -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- LOGO - Inicio -->
	<li class="nav-item active">
		<div
			class="sidebar-brand d-flex align-items-center justify-content-center">
			<form action="<%= UrlConstant.SERVLET__INIT %>" method="post">
				<div class="sidebar-brand-icon rotate-n-15">
					<i class="fas fa-laugh-wink"></i>
				</div>
				<input type="submit" class="btn-nav-link-admin" value="<%=Constant.APP_TITLE.toUpperCase()%>" />
			</form>
		</div>
	</li>

	<!-- Separador -->
	<hr class="sidebar-divider my-0">

	<!-- NAV Item - PANEL BACKEND -->
	<li class="nav-item active">
		<div class="nav-link">
			<form action="BackEnd" method="post">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<input type="submit" class="btn-nav-link-admin" value="BACKEND" />
			</form>
		</div>
	</li>

	<!-- Separador -->
	<hr class="sidebar-divider">

	<!-- T�tulo BACKEND -->
	<div class="sidebar-heading">Admin</div>

	<!-- NAV Item - ZAPATILLAS -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseZapatillas" aria-expanded="true" aria-controls="collapsePages">
			<i class="fas fa-fw fa-dice"></i> <span><fmt:message key="SIDEBAR__MENU__SHOE"/></span>
		</a>
		<div id="collapseZapatillas" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header"><fmt:message key="SIDEBAR__ITEM__MANTEINMENT"/></h6>
				<form action="<%= UrlConstant.SERVLET__SHOE_REGISTER %>" method="post">
					<input type="submit" class="collapse-item btn-collapse-item"
						value="<fmt:message key="SIDEBAR__SUBITEM_SHOE__CREATE"/>" />
				</form>
				<form action="#" method="post">
					<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_SHOE__EDIT"/></a>
				</form>
				<form action="#" method="post">
					<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_SHOE__DROP"/></a>
				</form>
				<div class="collapse-divider"></div>
				<h6 class="collapse-header"><fmt:message key="SIDEBAR__ITEM__QUERY"/></h6>
				<form action="#" method="post">
					<input type="submit" class="collapse-item btn-collapse-item"
						value="<fmt:message key="SIDEBAR__SUBITEM_SHOE__SHOW"/>" />
				</form>
					<div class="collapse-divider"></div>
			</div>
		</div>
	</li>

	<!-- NAV Item - USUARIOS -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsuarios" aria-expanded="true" aria-controls="collapsePages">
			<i class="fas fa-fw fa-users"></i> <span><fmt:message key="SIDEBAR__MENU__USER"/></span>
		</a>
		<div id="collapseUsuarios" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header"><fmt:message key="SIDEBAR__ITEM__MANTEINMENT"/></h6>
				<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_USER__CREATE"/></a>
				<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_USER__EDIT"/></a>
				<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_USER__DROP"/></a>
				<div class="collapse-divider"></div>
				<h6 class="collapse-header"><fmt:message key="SIDEBAR__ITEM__QUERY"/></h6>
				<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_USER__SHOW"/></a>
				<div class="collapse-divider"></div>
			</div>
		</div>
	</li>

	<!-- NAV Item - VENTAS -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVentas" aria-expanded="true" aria-controls="collapsePages">
			<i class="fas fa-fw fa-hand-holding-usd"></i> <span><fmt:message key="SIDEBAR__MENU__PURCHASE"/></span>
		</a>
		<div id="collapseVentas" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header"><fmt:message key="SIDEBAR__ITEM__QUERY"/></h6>
				<a class="collapse-item" href="#"><fmt:message key="SIDEBAR__SUBITEM_USER__SHOW"/></a>
				<div class="collapse-divider"></div>
			</div>
		</div>
	</li>

	<!-- Separador -->
	<hr class="sidebar-divider">
	
	<div class="text-center d-none d-md-inline">
		<a href="#" id="sidebarToggle" class="rounded-circle border-0 "></a>
	</div>

</ul>
<!-- FIN Barra Lateral -->


