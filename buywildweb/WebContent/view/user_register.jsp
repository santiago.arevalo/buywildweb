<%@page import="buywildweb.com.values.KeyConstant"%>
<%@page import="buywildweb.com.values.UrlConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="author" content="Santiago Arévalo Lavi">
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__BOOTSTRAP%>"/>
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__FONTAWESOME%>"/>
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__STYLES%>"/>
<link rel='stylesheet' type='text/css' href='<%=UrlConstant.CSS__USER_REGISTER%>'/>
<title>BWW-New User</title>
</head>
<body id="page-top">

	<!-- LANGUAGE FROM LOCALE -->
	<c:if test="${requestScope.locale == 'es'}">
		<c:set var="locale" value="es_ES" />
	</c:if>
	<c:if test="${requestScope.locale == 'en'}">
		<c:set var="locale" value="en_EN" />
	</c:if>
	<fmt:setLocale value="${locale}" scope="session" />
	<fmt:setBundle basename="string.Strings" scope="session" />

	<!-- wrapper page -->
	<div id="wrapper">
	
		<!-- SIDEBAR (if admin role) -->
		
		<!-- wrapper content -->
		<div id="content-wrapper" class="d-flex flex-column">
		
			<!-- main content -->
			<div id="content">
			
				<!-- HEADER -->
				<jsp:include page="header.jsp"></jsp:include>
				
				<!-- CONTENT -->
				<div class="cointainer-fluid">
				
					<!-- Form Title -->
					<h2><fmt:message key="USER_REGISTER__TITLE"/></h2>
					
					<div class="form-contenedor">
					
						<form action="<%=UrlConstant.SERVLET__USER_INSERT%>" method="post" accept-charset="utf-8">
				
							<div class="form-row">
								<div class="form-group col-12 col-sm-6">
									<label for="input_name"><fmt:message key="USER_REGISTER__USER_NAME"/>: </label>
									<input id="input_name" type="text" class="form-control" placeholder="<fmt:message key="USER_REGISTER__USER_NAME"/>" name="<%=KeyConstant.USER__NAME%>" required>
								</div>
								<div class="form-group col-12 col-sm-6">
									<label for="input_email"><fmt:message key="USER_REGISTER__USER_EMAIL"/>: </label>
									<input id="input_email" type="email" class="form-control" placeholder="<fmt:message key="USER_REGISTER__USER_EMAIL"/>" name="<%=KeyConstant.USER__EMAIL%>" required>
								</div>
							</div>
							
							<div class="form-row">
								<div class="form-group col-12 col-sm-6">
									<label for="input_password"><fmt:message key="USER_REGISTER__USER_PASS"/>: </label>
									<input id="input_password" type="password" class="form-control" placeholder="<fmt:message key="USER_REGISTER__USER_PASS"/>" name="<%=KeyConstant.USER__PASS_2%>" required>
								</div>
								<div class="form-group col-12 col-sm-6">
									<label for="input_password_2"><fmt:message key="USER_REGISTER__USER_CONFIRM_PASS"/>: </label>
									<input id="input_password_2" type="password" class="form-control" placeholder="<fmt:message key="USER_REGISTER__USER_PASS"/>" name="<%=KeyConstant.USER__PASS%>" required>
								</div>
							</div>
							
							<div class="form-row">
								<div class="col-12 col-sm-6 form-pie">
									<input type="submit" class="btn btn-primary" value="<fmt:message key="USER_REGISTER__USER_REGISTER"/>">
								</div>
								<div class="col-12 col-sm-6 form-pie">
									<input type="reset" class="btn btn-secondary" value="<fmt:message key="USER_REGISTER__USER_RESET"/>">
								</div>
							</div>
							
							<!-- Check info messages -->
							<div class="form-group response-message">
								<c:if test="${requestScope.error != null}">
									<label class="info-message">${requestScope.error}</label>
								</c:if>
							</div>
				
						</form>
				
					</div>
				
				</div>
				<!-- END CONTENT -->
			
			</div>
			<!-- END main content -->
		
		</div>
		<!-- END wrapper content -->
	
	</div>
	<!-- END wrapper page -->
	
	<!-- Botón Scroll al inicio TOP -->
	<a class="scroll-to-top rounded" href="#page-top">
	  <i class="fas fa-angle-up"></i>
	</a>

<script src="<%=UrlConstant.JS__JQUERY%>"></script>
<script src="<%=UrlConstant.JS__POPPER%>"></script>
<script src="<%=UrlConstant.JS__BOOTSTRAP%>"></script>
</body>
</html>