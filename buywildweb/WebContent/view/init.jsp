<%@page import="buywildweb.com.values.KeyConstant"%>
<%@ page import="buywildweb.com.values.UrlConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="author" content="Santiago Arévalo Lavi">
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__BOOTSTRAP%>">
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__FONTAWESOME%>">
<link rel="stylesheet" type="text/css" href="<%=UrlConstant.CSS__STYLES%>">
<link rel="stylesheet" type="text/css" href="resources/css/init.css">
<title>BWW-HOME</title>
</head>
<body>

	<!-- LANGUAGE FROM LOCALE -->
	<c:if test="${requestScope.locale == 'es'}">
		<c:set var="locale" value="es_ES" />
	</c:if>
	<c:if test="${requestScope.locale == 'en'}">
		<c:set var="locale" value="en_EN" />
	</c:if>
	<fmt:setLocale value="${locale}" scope="session" />
	<fmt:setBundle basename="string.Strings" scope="session" />
	
	<div class="container">

		<div class="d-flex justify-content-center h-100">

			<!-- CARD FORM -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">
						<fmt:message key="INIT_FORM__TITLE" />
					</h3>
				</div>
				<article class="card-body mx-auto" style="max-width: 400px;">
					<form action="<%=UrlConstant.SERVLET__INIT_RES%>" method="post" accept-charset="UTF-8">

						<!-- HOST -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-cloud"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__HOST%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_HOST_NAME"/>"
								type="text" required>
						</div>

						<!-- PORT -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-plug"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__PORT%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_PORT"/>"
								type="number" required>
						</div>

						<!-- DATABASE -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-database"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__DATABASE%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_DATABASE_NAME"/>"
								type="text" required>
						</div>

						<!-- SCHEMA -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fas fa-coins"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__SCHEMA%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_SCHEMA"/>"
								type="text">
						</div>

						<!-- USER -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-user"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__USER%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_USER_NAME"/>"
								type="text" required>
						</div>

						<!-- PASSWORD -->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-lock"></i>
								</span>
							</div>
							<input name="<%=KeyConstant.SERVER__PASSWORD%>" class="form-control"
								placeholder="<fmt:message key="INIT_FORM__HOLDER_USER_PASSWORD"/>"
								type="password" required>
						</div>

						<!-- SUBMIT -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">
								<fmt:message key="INIT_FORM__ACCESS" />
							</button>
						</div>

						<!-- Check info messages -->
						<div class="form-group response-message">
							<c:if test="${requestScope.error != null}">
								<label class="info-message">${requestScope.error}</label>
							</c:if>
						</div>
						
					</form>
				</article>
			</div>

		</div>

	</div>

<script src="<%=UrlConstant.JS__JQUERY%>"></script>
<script src="<%=UrlConstant.JS__POPPER%>"></script>
<script src="<%=UrlConstant.JS__BOOTSTRAP%>"></script>
</body>
</html>